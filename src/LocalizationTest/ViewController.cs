﻿using Foundation;
using System;
using UIKit;

namespace LocalizationTest
{
    public partial class ViewController : UIViewController
    {
        private int _counter;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ShowClicksText();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            btnClickMe.TouchUpInside += BtnClickMeOnTouchUpInside;
        }


        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            btnClickMe.TouchUpInside -= BtnClickMeOnTouchUpInside;
        }

        private void BtnClickMeOnTouchUpInside(object sender, EventArgs e)
        {
            _counter++;

            ShowClicksText();
        }

        private void ShowClicksText()
        {
            var format = NSBundle.MainBundle.GetLocalizedString("click-count");

            lblCounterText.Text = NSString.LocalizedFormat(format, (NSNumber) _counter);
        }
    }
}
